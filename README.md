
## Running the program
1)

```bash
python main.py 8191
```

The above example should return '7F7F' as expected.

2)

```bash
python main.py -d 7F -y 7F
```

The above example returns '8191' which is our original integer.