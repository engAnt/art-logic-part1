from encode_decode import *

to_encode = [6111, 340, -2628, -255, 7550]
to_decode = [(0x0A, 0x0A), (0x29, 0x00), (0x0F, 0x3F), (0x00, 0x44), (0x7F, 0x5E)]

with open("ConvertedData.txt", "w") as wfile:
    wfile.write( f'Given the foloowing integers =>\n' )
    wfile.write( f'6111\n' )
    wfile.write( f'340\n' )
    wfile.write( f'-2628\n' )
    wfile.write( f'-255\n' )
    wfile.write( f'7550\n\n' )
    wfile.write( f'These are the encoded values:\n' )

    for val in to_encode:
        wfile.write(f'{encode(val)}\n')
    
    wfile.write(f"\n{'*****' * 16}\n\n")

    wfile.write( f'Given the following hexadecimal values =>\n' )
    wfile.write( f'0A0A\n' )
    wfile.write( f'0029\n' )
    wfile.write( f'3F0F\n' )
    wfile.write( f'4400\n' )
    wfile.write( f'5E7F\n\n' )
    wfile.write( f'These are the decoded integers:\n' )

    for lowB, highB in to_decode:
        wfile.write(f'{decode(lowB, highB)}\n')