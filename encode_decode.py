
def encode(unencoded_decimal):
    encoded_decimal = unencoded_decimal + 8192
    low_bits = encoded_decimal & 0x007F
    high_bits = (encoded_decimal & 0x3F80) << 1
    encoded_hex = low_bits + high_bits
    
    hex_str = f'{encoded_hex:04X}'
    return hex_str


def decode(low_byte, hi_byte):
    return (low_byte | (hi_byte << 7)) - 8192