import argparse
from encode_decode import *



def Main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--encode", help="encode a given integer", action="store_true")
    parser.add_argument("-d", "--decode", help="decode a hexadecimal value (low and high byte values are passes in respectively)", action="store_true")
    parser.add_argument("x", help="first value (integer or low hex)")
    parser.add_argument("-y", help="optional, high hex value")

    try:
        cmd_args = parser.parse_args()
        #print(cmd_args)

        if cmd_args.decode:
            return decode(int(cmd_args.x, 16), int(cmd_args.y, 16))
        else:
            # default is encode(x)
            return encode(int(cmd_args.x))
    except Exception as err:
        print("Parser or other exception occured")
        print(err)


if __name__ == '__main__':
    print(f'{Main()}')