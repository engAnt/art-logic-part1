import pytest
from encode_decode import *

def test_decoding_1():
    assert decode(0x00, 0x40) == 0

def test_decoding_2():
    assert decode(0x00, 0x00) == -8192

def test_decoding_3():
    assert decode(0x7F, 0x7F) == 8191

def test_decoding_4():
    assert decode(0x00, 0x50) == 2048

def test_decoding_5():
    assert decode(0x05, 0x0A) == -6907

def test_decoding_6():
    assert decode(0x00, 0x55) == 2688
