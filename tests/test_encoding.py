import pytest
from encode_decode import *

def test_encoding_1():
    assert encode(0) == "4000"

def test_encoding_2():
    assert encode(-8192) == "0000"

def test_encoding_3():
    assert encode(8191) == "7F7F"

def test_encoding_4():
    assert encode(2048) == "5000"

def test_encoding_5():
    assert encode(-4096) == "2000"